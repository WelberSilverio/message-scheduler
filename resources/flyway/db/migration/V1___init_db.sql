CREATE TABLE message_status (
   id BIGINT AUTO_INCREMENT PRIMARY KEY,
   description VARCHAR(64) NOT NULL,
   created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO message_status (id, description)
   VALUES
     (1, 'WAITING'),
     (2, 'SENT'),
     (3, 'CANCELED');

CREATE TABLE message_entity (
   id BIGINT AUTO_INCREMENT PRIMARY KEY,
   message VARCHAR(64) NOT NULL,
   message_channel VARCHAR(64) NOT NULL,
   recipient VARCHAR(64) NOT NULL,
   message_status_id BIGINT NOT NULL,
   schedule_date TIMESTAMP NOT NULL,
   created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   FOREIGN KEY (message_status_id)
            REFERENCES message_status (id)
);

CREATE TABLE message_history (
   id BIGINT AUTO_INCREMENT PRIMARY KEY,
   message_id BIGINT NOT NULL,
   success BOOLEAN NOT NULL,
   details VARCHAR(64),
   created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   FOREIGN KEY (message_id)
               REFERENCES message_entity (id)
);
