package br.com.posts;

import br.com.posts.controller.config.CustomObjectMapperConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.jackson.ObjectMapperCustomizer;
import javax.inject.Singleton;

@Singleton
public class ObjectMapperConfig implements ObjectMapperCustomizer {

  public void customize(final ObjectMapper mapper) {
    CustomObjectMapperConfig.customize(mapper);
  }

}
