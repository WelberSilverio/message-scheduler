# **Scheduler-Api**
Scheduler-Api é o ponto de entrada para agendamento e envio de mensagens. 


## Sobre a API:

Worker tem como responsabilidade agendar envios de mensagens.


# Sumário

- [Pré-requisitos](#Pré-requisitos)
- [Configuração e execução](#Configuração e execução)
- [Testes](#Testes)


# Pré-requisitos

- [Java-11](https://sdkman.io/install)
- [Plugin Lombok](https://projectlombok.org)
- [Docker](https://www.docker.com/)

# Configuração e execução

**Faça um clone do projeto:**

https<br/>
https://gitlab.com/WelberSilverio/message-scheduler.git

ssh<br/>
git@gitlab.com:WelberSilverio/message-scheduler.git

**Crie o build e imagem Docker da aplicação :**<br/>
```
$ mvn clean package
```

```
$ docker build  -t posts/app-quarkus-jvm .
```

**Levante o cenário dependências do projeto:**<br/>

```
$ docker-compose -f .docker-compose/stack.yml up
```

**Levante a aplicação :**<br/>

```
$ docker-compose -f .docker-compose/quarkus-app.yml up
```

# Testes
Foram criados testes de Cobertura de código (linha e condições) e Mutação,  onde 100% do código deve estar coberto.

**Rodar baterias de testes:**
```
$ mvn clean test
```
