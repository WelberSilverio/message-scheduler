package br.com.posts.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class MessageStatusTest {

  @Test
  public void testFindStatusById() {
    var status = MessageStatus.getValue(1L);
    assertThat(status).isEqualTo(MessageStatus.WAITING);
  }

  @Test
  public void testFindStatusNonExistent() {
    var status = MessageStatus.getValue(7L);
    assertThat(status).isNull();
  }

  @Test
  public void testGetStatusId() {
    var status = MessageStatus.WAITING.getId();
    assertThat(status).isEqualTo(1L);
  }
}