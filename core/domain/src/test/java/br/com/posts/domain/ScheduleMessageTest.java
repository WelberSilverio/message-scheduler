package br.com.posts.domain;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ScheduleMessageTest {

  @Test
  public void testCreateSchedulerMessageWithRecipientNull() {
    var builder = ScheduleMessage.builder()
        .id(1L);

    assertThatThrownBy(builder::build)
        .hasMessage("Recipient of message cant be null");
  }

  @Test
  public void testCreateSchedulerMessageWithMessageTypeNull() {
    var builder = ScheduleMessage.builder()
        .id(1L).recipient("any");

    assertThatThrownBy(builder::build)
        .hasMessage("Type of message cant be null");
  }

  @Test
  public void testCreateSchedulerMessageWithTypeSMSButRecipientIsNotPhoneNumber() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.SMS).recipient("not_number");

    assertThatThrownBy(builder::build)
        .hasMessage("A phone number was expected but it wasn't!");
  }

  @Test
  public void testCreateSchedulerMessageWithTypeWHATSAPPButRecipientIsNotPhoneNumber() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.WHATSAPP).recipient("not_number");

    assertThatThrownBy(builder::build)
        .hasMessage("A phone number was expected but it wasn't!");
  }

  @Test
  public void testCreateSchedulerMessageWithTypeEMAILButRecipientIsNotAEmail() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.EMAIL).recipient("not_email");

    assertThatThrownBy(builder::build)
        .hasMessage("Message type is EMAIL but recipient is not!");
  }

  @Test
  public void testCreateSchedulerMessageWithTypeEMAILButMessageBodyIsNull() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.EMAIL).recipient("any@email.com");

    assertThatThrownBy(builder::build)
        .hasMessage("MessageBody cant be null");
  }

  @Test
  public void testCreateSchedulerMessageWithTypeEMAILButMessageBodyIsEmpty() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.EMAIL)
        .recipient("any@email.com")
        .messageBody("");

    assertThatThrownBy(builder::build)
        .hasMessage("MessageBody cant be empty or blank");
  }

  @Test
  public void testCreateSchedulerMessageWithTypeEMAILButMessageBodyIsBlank() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.EMAIL)
        .recipient("any@email.com")
        .messageBody("   ");

    assertThatThrownBy(builder::build)
        .hasMessage("MessageBody cant be empty or blank");
  }

  @Test
  public void testCreateSchedulerMessageWithSucces() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.EMAIL)
        .recipient("any@email.com")
        .messageBody(" Any message  ")
        .messageStatus(MessageStatus.WAITING);

    assertThat(builder.build()).isNotNull();
  }

  @Test
  public void testCreateSchedulerMessageWithSuccesWhenTypeIsSMS() {
    var builder = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.SMS)
        .recipient("19666666666")
        .messageBody(" Any message  ")
        .messageStatus(MessageStatus.WAITING);

    assertThat(builder.build()).isNotNull();
  }

  @Test
  public void dummyTestEqualsAndHashCodeAndToString() {
    var schedule1 = ScheduleMessage.builder()
        .id(1L).typeMessage(TypeMessage.EMAIL)
        .recipient("any@email.com")
        .messageBody(" Any message  ")
        .messageStatus(MessageStatus.WAITING).build();

    var schedule2 = ScheduleMessage.builder()
        .id(2L).typeMessage(TypeMessage.PUSH)
        .recipient("anyThing")
        .messageBody("Any message")
        .messageStatus(MessageStatus.WAITING).build();

    assertThat(schedule1.equals(schedule2)).isFalse();
    assertThat(schedule1.toString()).isNotNull();
  }

}