package br.com.posts.domain.exception;

public class InvalidMessageBodyException extends ProcessException {
  public InvalidMessageBodyException(final String message) {
    super(message);
  }
}
