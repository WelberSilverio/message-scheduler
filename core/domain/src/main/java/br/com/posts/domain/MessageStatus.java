package br.com.posts.domain;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MessageStatus {

  WAITING(1L),
  SENT(2L),
  CANCELED(3L);

  private final long id;

  public static MessageStatus getValue(final long value) {
    return Arrays.stream(MessageStatus.values())
        .filter(e -> e.id == value)
        .findFirst().orElse(null);
  }
}
