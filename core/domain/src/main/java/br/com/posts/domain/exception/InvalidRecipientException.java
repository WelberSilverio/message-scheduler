package br.com.posts.domain.exception;

public class InvalidRecipientException extends ProcessException {
  public InvalidRecipientException(final String message) {
    super(message);
  }
}
