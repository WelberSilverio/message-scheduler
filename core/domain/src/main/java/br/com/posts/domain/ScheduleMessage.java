package br.com.posts.domain;

import br.com.posts.domain.exception.InvalidMessageBodyException;
import br.com.posts.domain.exception.InvalidRecipientException;
import java.time.ZonedDateTime;
import java.util.Objects;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class ScheduleMessage {

  private final long id;
  private final ZonedDateTime scheduleDate;
  private final String messageBody;
  private final String recipient;
  private final TypeMessage typeMessage;
  private final MessageStatus messageStatus;

  @Builder
  public ScheduleMessage(final long id,
                         final ZonedDateTime scheduleDate,
                         final String messageBody,
                         final String recipient,
                         final TypeMessage typeMessage,
                         final MessageStatus messageStatus) {
    this.validateRecipient(recipient, typeMessage);
    this.validateMessageBody(messageBody);
    this.id = id;
    this.scheduleDate = scheduleDate;
    this.messageBody = messageBody;
    this.recipient = recipient;
    this.typeMessage = typeMessage;
    this.messageStatus = messageStatus;
  }

  private void validateRecipient(final String messageRecipient,
                                 final TypeMessage messageType) {
    Objects.requireNonNull(messageRecipient, "Recipient of message cant be null");
    Objects.requireNonNull(messageType, "Type of message cant be null");

    if ((TypeMessage.SMS.equals(messageType)
           || TypeMessage.WHATSAPP.equals(messageType))
           && !messageRecipient.chars().allMatch(Character::isDigit)) {
      throw new InvalidRecipientException("A phone number was expected but it wasn't!");
    } else if (TypeMessage.EMAIL.equals(messageType) && !messageRecipient.contains("@")) {
      throw new InvalidRecipientException("Message type is EMAIL but recipient is not!");
    }
  }

  private void validateMessageBody(final String message) {
    Objects.requireNonNull(message, "MessageBody cant be null");
    if (message.isEmpty() || message.isBlank()) {
      throw new InvalidMessageBodyException("MessageBody cant be empty or blank");
    }
  }
}
