package br.com.posts.domain;

public enum TypeMessage {
  WHATSAPP,
  EMAIL,
  SMS,
  PUSH
}
