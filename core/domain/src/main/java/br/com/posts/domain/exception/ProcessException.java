package br.com.posts.domain.exception;

public class ProcessException extends RuntimeException {
  public ProcessException(final String message) {
    super(message);
  }
}
