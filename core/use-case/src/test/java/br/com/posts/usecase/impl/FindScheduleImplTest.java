package br.com.posts.usecase.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.domain.TypeMessage;
import br.com.posts.usecase.gateway.ScheduleMessageGateway;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class FindScheduleImplTest {

  @Mock
  private ScheduleMessageGateway scheduleMessageGateway;

  @InjectMocks
  private FindScheduleImpl findSchedule;

  static final ScheduleMessage  scheduleMessage = ScheduleMessage.builder()
      .id(1L).typeMessage(TypeMessage.EMAIL)
      .recipient("any@email.com")
      .messageBody("Any message")
      .messageStatus(MessageStatus.WAITING).build();

  @Test
  public void testFindWithSuccessSchedule() {

    when(scheduleMessageGateway.find(1L))
        .thenReturn(Optional.of(scheduleMessage));

    var result = findSchedule.find(1L);
    assertThat(result).isEqualTo(scheduleMessage);
    verify(scheduleMessageGateway).find(1L);
  }

  @Test
  public void testFindWithFailSchedule() {

    when(scheduleMessageGateway.find(1L))
        .thenReturn(Optional.empty());

    assertThatThrownBy(()-> findSchedule.find(1L))
        .hasMessage("Schedule of message not found");
    verify(scheduleMessageGateway).find(1L);
  }
}