package br.com.posts.usecase.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.posts.usecase.gateway.ScheduleMessageGateway;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CancelScheduleImplTest {

  @Mock
  private ScheduleMessageGateway scheduleMessageGateway;

  @InjectMocks
  private CancelScheduleImpl cancelSchedule;

  @Test
  public void testCancelScheduleUseCase() {
    cancelSchedule.cancel(1L);
    verify(scheduleMessageGateway).cancel(1L);
  }
}