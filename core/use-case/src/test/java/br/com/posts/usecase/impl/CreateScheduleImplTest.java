package br.com.posts.usecase.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.domain.TypeMessage;
import br.com.posts.usecase.gateway.ScheduleMessageGateway;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CreateScheduleImplTest {

  @Mock
  private ScheduleMessageGateway scheduleMessageGateway;

  @InjectMocks
  private CreateScheduleImpl createSchedule;

  static final ScheduleMessage scheduleMessage = ScheduleMessage.builder()
      .id(1L).typeMessage(TypeMessage.EMAIL)
      .recipient("any@email.com")
      .messageBody("Any message")
      .messageStatus(MessageStatus.WAITING).build();

  @Test
  public void testCreateScheduleWithSuccess() {

    when(scheduleMessageGateway.create(any(),any()))
        .thenReturn(scheduleMessage);

    var result = createSchedule.create(scheduleMessage);
    assertThat(result).isEqualTo(scheduleMessage);
    verify(scheduleMessageGateway).create(any(), any());
  }

}