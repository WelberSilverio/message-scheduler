package br.com.posts.usecase.gateway;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import java.util.Optional;

public interface ScheduleMessageGateway {
  ScheduleMessage create(final ScheduleMessage scheduleMessage,
                         final MessageStatus messageStatus);

  Optional<ScheduleMessage> find(final long id);

  void cancel(final long id);
}
