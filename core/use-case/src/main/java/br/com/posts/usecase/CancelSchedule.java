package br.com.posts.usecase;

public interface CancelSchedule {
  void cancel(final long id);
}
