package br.com.posts.usecase.impl;

import br.com.posts.usecase.CancelSchedule;
import br.com.posts.usecase.gateway.ScheduleMessageGateway;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CancelScheduleImpl implements CancelSchedule {

  private final ScheduleMessageGateway scheduleMessageGateway;

  @Inject
  public CancelScheduleImpl(final ScheduleMessageGateway scheduleMessageGateway) {
    this.scheduleMessageGateway = scheduleMessageGateway;
  }

  @Override
  public void cancel(final long id) {
    scheduleMessageGateway.cancel(id);
  }
}
