package br.com.posts.usecase;

import br.com.posts.domain.ScheduleMessage;

public interface CreateSchedule {

  ScheduleMessage create(final ScheduleMessage scheduleMessage);

}
