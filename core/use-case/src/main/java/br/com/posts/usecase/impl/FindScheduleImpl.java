package br.com.posts.usecase.impl;

import br.com.posts.domain.ScheduleMessage;
import br.com.posts.usecase.FindSchedule;
import br.com.posts.usecase.exception.ScheduleMessageNotFoundException;
import br.com.posts.usecase.gateway.ScheduleMessageGateway;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class FindScheduleImpl implements FindSchedule {

  private final ScheduleMessageGateway scheduleMessageGateway;

  @Inject
  public FindScheduleImpl(final ScheduleMessageGateway scheduleMessageGateway) {
    this.scheduleMessageGateway = scheduleMessageGateway;
  }


  @Override
  public ScheduleMessage find(final long id) {
    return scheduleMessageGateway.find(id)
        .orElseThrow(() -> new ScheduleMessageNotFoundException("Schedule of message not found"));
  }
}
