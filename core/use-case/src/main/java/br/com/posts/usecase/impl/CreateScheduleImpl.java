package br.com.posts.usecase.impl;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.usecase.CreateSchedule;
import br.com.posts.usecase.gateway.ScheduleMessageGateway;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CreateScheduleImpl implements CreateSchedule {

  private final ScheduleMessageGateway scheduleMessageGateway;

  @Inject
  public CreateScheduleImpl(final ScheduleMessageGateway scheduleMessageGateway) {
    this.scheduleMessageGateway = scheduleMessageGateway;
  }

  @Override
  public ScheduleMessage create(final ScheduleMessage scheduleMessage) {
    return scheduleMessageGateway.create(scheduleMessage, MessageStatus.WAITING);
  }
}
