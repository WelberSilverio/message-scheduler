package br.com.posts.usecase;

import br.com.posts.domain.ScheduleMessage;

public interface FindSchedule {
  ScheduleMessage find(final long id);
}
