package br.com.posts.usecase.exception;

public class ScheduleMessageNotFoundException extends RuntimeException {
  public ScheduleMessageNotFoundException(final String message) {
    super(message);
  }
}
