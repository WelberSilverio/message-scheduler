package br.com.posts.repository.entity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MessageEntityTest {

  @Test
  public void dummyTestNoArgsAllArgsAndEquals() {
    var entity1 = new MessageEntity();
    var entity2 = new MessageEntity(null, null,null,null,null);

    assertThat(entity1.equals(entity2)).isTrue();
  }
}