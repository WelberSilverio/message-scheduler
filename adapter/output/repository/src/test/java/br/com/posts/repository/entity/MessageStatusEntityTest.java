package br.com.posts.repository.entity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MessageStatusEntityTest {

  @Test
  public void dummyTestMessageStatusEntity() {
    var entity1 = new MessageStatusEntity();
    var entity2 = new MessageStatusEntity((String) null);

    var entity3 = MessageStatusEntity.builder().build();

    entity3.setDescription("SENT");

    assertThat(entity1.equals(entity2)).isTrue();
    assertThat(entity3.getDescription()).isNotNull();
  }
}