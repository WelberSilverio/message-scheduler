package br.com.posts.repository.entity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;

@ExtendWith(MockitoExtension.class)
class BaseEntityTest {

  @Test
  public void dummyTestBaseEntity() {
    var entity = mock(
        BaseEntity.class,
        Mockito.CALLS_REAL_METHODS);

    var entity2 = mock(
        BaseEntity.class,
        Mockito.withSettings()
        .useConstructor(999L, ZonedDateTime.now(), ZonedDateTime.now())
        .defaultAnswer(Mockito.CALLS_REAL_METHODS));

    entity.setId(1L);
    assertThat(entity2.getCreatedAt()).isNotNull();
    assertThat(entity2.getUpdatedAt()).isNotNull();
    assertThat(entity.equals(entity2)).isFalse();
  }

  @Test
  public void prePersistWhenAllDateIsNull() {
    var entity = mock(
        BaseEntity.class,
        Mockito.CALLS_REAL_METHODS);

    entity.prePersist();

    assertThat(entity.getCreatedAt()).isNotNull();
    assertThat(entity.getUpdatedAt()).isNotNull();
  }

  @Test
  public void preUpdateWhenAllDateIsNull() {
    var entity = mock(
        BaseEntity.class,
        Mockito.CALLS_REAL_METHODS);

    entity.preUpdate();

    assertThat(entity.getCreatedAt()).isNull();
    assertThat(entity.getUpdatedAt()).isNotNull();
  }

  @Test
  public void prePersistWhenAllDateExists() {
    var entity =mock(
        BaseEntity.class,
        Mockito.withSettings()
            .useConstructor(999L, ZonedDateTime.now(), ZonedDateTime.now())
            .defaultAnswer(Mockito.CALLS_REAL_METHODS));

    entity.prePersist();

    assertThat(entity.getCreatedAt()).isNotNull();
    assertThat(entity.getUpdatedAt()).isNotNull();
  }

}