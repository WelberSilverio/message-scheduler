package br.com.posts.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatNoException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.domain.TypeMessage;
import br.com.posts.repository.entity.MessageEntity;
import br.com.posts.repository.mapper.RepositoryScheduleMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ScheduleMessageGatewayImplTest {

  @Mock
  private MessageEntityRepository repository;

  @Spy
  private RepositoryScheduleMapper repositoryScheduleMapper = new RepositoryScheduleMapper();

  @InjectMocks
  private ScheduleMessageGatewayImpl scheduleMessageGateway;

  static final ScheduleMessage scheduleMessage = ScheduleMessage.builder()
      .id(1L).typeMessage(TypeMessage.EMAIL)
      .recipient("any@email.com")
      .messageBody("Any message")
      .scheduleDate(ZonedDateTime.now())
      .messageStatus(MessageStatus.WAITING).build();

  static final MessageEntity.MessageEntityBuilder messageEntity = MessageEntity.builder()
      .messageBody(scheduleMessage.getMessageBody())
      .messageStatus(scheduleMessage.getMessageStatus().getId())
      .messageChannel(scheduleMessage.getTypeMessage().name())
      .id(1L)
      .messageStatus(1L)
      .recipient(scheduleMessage.getRecipient())
      .scheduleDate(scheduleMessage.getScheduleDate());

  @Test
  public void testFindScheduleSuccess(){
    when(repository.find(1L)).thenReturn(Optional.of(messageEntity.build()));
    var result = scheduleMessageGateway.find(1L);
    assertThat(result.get()).isNotNull();
    verify(repository).find(1L);
    verify(repositoryScheduleMapper).toDomain(messageEntity.build());
  }

  @Test
  public void testCancelScheduleFail(){
    when(repository.find(1L)).thenReturn(Optional.empty());
    assertThatThrownBy(() -> scheduleMessageGateway.cancel(1L))
        .hasMessage("Schedule of message not found");
    verify(repository).find(1L);
    verify(repositoryScheduleMapper, never()).toDomain(any());
  }

  @Test
  public void testCancelScheduleSuccess(){
    var message = messageEntity.build();
    when(repository.find(1L)).thenReturn(Optional.of(message));
    assertThatNoException().isThrownBy(()-> scheduleMessageGateway.cancel(1L));
    assertThat(message.getMessageStatus()).isEqualTo(MessageStatus.CANCELED.getId());
    verify(repository).find(1L);
    verify(repository).save(any());
  }

  @Test
  public void testSaveScheduleSuccess(){
    var result = scheduleMessageGateway.create(scheduleMessage, MessageStatus.WAITING);
    assertThat(result).isNotNull();
    verify(repository).save(any());
    verify(repositoryScheduleMapper).toEntity(any());
    verify(repositoryScheduleMapper).toDomain(any());
  }
}