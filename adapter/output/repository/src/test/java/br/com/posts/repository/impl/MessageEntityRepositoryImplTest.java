package br.com.posts.repository.impl;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.posts.repository.entity.MessageEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import java.time.ZonedDateTime;

@ExtendWith(MockitoExtension.class)
public class MessageEntityRepositoryImplTest {

  @Mock
  private EntityManager entityManager;

  @InjectMocks
  MessageEntityRepositoryImpl messageEntityRepository;

  static final MessageEntity messageEntity = MessageEntity.builder()
      .messageBody("Any Message")
      .messageStatus(1L)
      .messageChannel("EMAIL")
      .id(1L)
      .messageStatus(1L)
      .recipient("test@test")
      .scheduleDate(ZonedDateTime.now())
      .build();

  @Test
  public void testSaveEntity() {
    assertThatNoException().isThrownBy(()-> messageEntityRepository.save(messageEntity));
    verify(entityManager).persist(messageEntity);
  }

  @Test
  public void testFindEntity() {
    when(entityManager.find(any(),any())).thenReturn(messageEntity);
    var response = messageEntityRepository.find(1L);
    assertThat(response.get()).isEqualTo(messageEntity);
    verify(entityManager).find(MessageEntity.class, 1L);
  }
}