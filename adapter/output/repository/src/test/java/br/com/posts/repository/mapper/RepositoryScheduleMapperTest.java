package br.com.posts.repository.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.domain.TypeMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;

@ExtendWith(MockitoExtension.class)
public class RepositoryScheduleMapperTest {

  private RepositoryScheduleMapper repositoryScheduleMapper = new RepositoryScheduleMapper();

  @Test
  public void testParseObjectWhenIdIsNull() {
    var scheduleMessage = ScheduleMessage.builder()
        .typeMessage(TypeMessage.EMAIL)
        .recipient("any@email.com")
        .messageBody("Any message")
        .scheduleDate(ZonedDateTime.now())
        .messageStatus(MessageStatus.WAITING).build();

    var result = repositoryScheduleMapper.toEntity(scheduleMessage);

    assertThat(result.getId()).isNull();
  }
}