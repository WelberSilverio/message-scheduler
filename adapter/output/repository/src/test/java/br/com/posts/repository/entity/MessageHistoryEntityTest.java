package br.com.posts.repository.entity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MessageHistoryEntityTest {

  @Test
  public void dummyTestMessageHistoryEntity() {
    var entity1 = new MessageHistoryEntity();
    var entity2 = new MessageHistoryEntity(null,null,null);

    var entity3 = MessageHistoryEntity.builder().build();

    entity3.setMessageId(1L);
    entity3.setDetails("Message send with success!");
    entity3.setSuccess(Boolean.TRUE);

    assertThat(entity1.equals(entity2)).isTrue();
    assertThat(entity3.getDetails()).isNotNull();
  }
}