package br.com.posts.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "message_history")
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MessageHistoryEntity extends BaseEntity {

  @Column(name = "message_id")
  private Long messageId;

  @Column(name = "success")
  private Boolean success;

  @Column(name = "details")
  private String details;

}
