package br.com.posts.repository.mapper;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.domain.TypeMessage;
import br.com.posts.repository.entity.MessageEntity;
import java.util.Objects;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RepositoryScheduleMapper {

  public MessageEntity toEntity(final ScheduleMessage scheduleMessage) {
    return MessageEntity.builder()
        .id((scheduleMessage.getId() <= 0) ? null : scheduleMessage.getId())
        .messageBody(scheduleMessage.getMessageBody())
        .scheduleDate(scheduleMessage.getScheduleDate())
        .messageChannel(scheduleMessage.getTypeMessage().name())
        .recipient(scheduleMessage.getRecipient())
        .build();
  }

  public ScheduleMessage toDomain(final MessageEntity messageEntity) {
    return ScheduleMessage.builder()
        .id(messageEntity.getId())
        .messageBody(messageEntity.getMessageBody())
        .typeMessage(TypeMessage.valueOf(messageEntity.getMessageChannel()))
        .recipient(messageEntity.getRecipient())
        .scheduleDate(messageEntity.getScheduleDate())
        .messageStatus(MessageStatus.getValue(messageEntity.getMessageStatus()))
        .build();
  }
}
