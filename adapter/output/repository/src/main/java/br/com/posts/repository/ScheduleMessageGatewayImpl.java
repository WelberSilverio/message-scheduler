package br.com.posts.repository;

import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.repository.mapper.RepositoryScheduleMapper;
import br.com.posts.usecase.exception.ScheduleMessageNotFoundException;
import br.com.posts.usecase.gateway.ScheduleMessageGateway;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ScheduleMessageGatewayImpl implements ScheduleMessageGateway {

  private final RepositoryScheduleMapper mapper;
  private final MessageEntityRepository repository;

  @Inject
  public ScheduleMessageGatewayImpl(final RepositoryScheduleMapper mapper,
                                    final MessageEntityRepository repository) {
    this.mapper = mapper;
    this.repository = repository;
  }

  @Override
  public ScheduleMessage create(final ScheduleMessage scheduleMessage,
                                final MessageStatus messageStatus) {
    var message = mapper.toEntity(scheduleMessage);
    message.setMessageStatus(messageStatus.getId());

    repository.save(message);

    return mapper.toDomain(message);
  }

  @Override
  public Optional<ScheduleMessage> find(final long id) {
    return repository.find(id).map(mapper::toDomain);
  }

  @Override
  public void cancel(final long id) {
    var message = repository.find(id)
        .orElseThrow(() -> new ScheduleMessageNotFoundException("Schedule of message not found"));
    message.setMessageStatus(MessageStatus.CANCELED.getId());
    repository.save(message);
  }
}
