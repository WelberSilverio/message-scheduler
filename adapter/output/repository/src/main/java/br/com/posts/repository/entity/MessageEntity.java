package br.com.posts.repository.entity;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "message_entity")
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MessageEntity extends BaseEntity {

  @Column(name = "message")
  private String messageBody;

  @Column(name = "message_channel")
  private String messageChannel;

  @Column(name = "recipient")
  private String recipient;

  @Column(name = "message_status_id")
  private Long messageStatus;

  @Column(name = "schedule_date")
  private ZonedDateTime scheduleDate;

}
