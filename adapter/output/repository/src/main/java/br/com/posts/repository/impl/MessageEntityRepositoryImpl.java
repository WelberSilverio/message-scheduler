package br.com.posts.repository.impl;

import br.com.posts.repository.MessageEntityRepository;
import br.com.posts.repository.entity.MessageEntity;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@ApplicationScoped
public class MessageEntityRepositoryImpl implements MessageEntityRepository {

  private final EntityManager entityManager;

  @Inject
  public MessageEntityRepositoryImpl(final EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  @Override
  @Transactional
  public void save(final MessageEntity message) {
    entityManager.persist(message);
  }

  @Override
  public Optional<MessageEntity> find(final Long id) {
    return Optional
       .ofNullable(entityManager.find(MessageEntity.class, id));
  }
}
