package br.com.posts.repository;

import br.com.posts.repository.entity.MessageEntity;
import java.util.Optional;

public interface MessageEntityRepository {
  void save(final MessageEntity message);

  Optional<MessageEntity> find(final Long id);
}
