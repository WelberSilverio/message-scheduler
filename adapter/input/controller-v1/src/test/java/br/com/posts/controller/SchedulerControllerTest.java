package br.com.posts.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import br.com.posts.controller.dto.ScheduleCreateDto;
import br.com.posts.controller.dto.TypeMessageDto;
import br.com.posts.controller.mapper.ControllerSchedullerMapper;
import br.com.posts.domain.MessageStatus;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.domain.TypeMessage;
import br.com.posts.usecase.CancelSchedule;
import br.com.posts.usecase.CreateSchedule;
import br.com.posts.usecase.FindSchedule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;

@ExtendWith(MockitoExtension.class)
public class SchedulerControllerTest {

  @Mock
  private CreateSchedule createSchedule;

  @Mock
  private FindSchedule findSchedule;

  @Mock
  private CancelSchedule cancelSchedule;

  @Spy
  private final ControllerSchedullerMapper mapper = new ControllerSchedullerMapper();

  @InjectMocks
  private SchedulerController schedulerController;

  @Test
  public void testCreateScheduleWithSuccess() {
    var body = "Any Message";
    var now = ZonedDateTime.now();
    var recipient = "16999999999";
    var scheduleDto = ScheduleCreateDto.builder()
        .messageBody(body)
        .scheduleDate(now)
        .typeMessage(TypeMessageDto.SMS)
        .recipient(recipient)
        .build();
    when(createSchedule.create(any()))
        .thenReturn(ScheduleMessage.builder()
            .messageBody(body)
            .messageStatus(MessageStatus.WAITING)
            .typeMessage(TypeMessage.SMS)
            .scheduleDate(now)
            .id(1L)
            .recipient(recipient)
            .build());
    var response = schedulerController.create(scheduleDto);
    verify(mapper).toDomain(any());
    verify(mapper).toResponseDto(any());
    verify(createSchedule).create(any());
    assertThat(response.getStatus()).isEqualTo(201);
    assertThat(response.getEntity()).isNotNull();
  }

  @Test
  public void testFindScheduleWithSuccess() {
    var id =  1L;
    when(findSchedule.find(id))
        .thenReturn(ScheduleMessage.builder()
            .messageBody("Any Message")
            .messageStatus(MessageStatus.WAITING)
            .typeMessage(TypeMessage.SMS)
            .scheduleDate(ZonedDateTime.now())
            .id(1L)
            .recipient("16999999999")
            .build());
    var response = schedulerController.get(id);
    verify(findSchedule).find(id);
    verify(mapper).toResponseDto(any());
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(response.getEntity()).isNotNull();
  }

  @Test
  public void testCancelScheduleWithSuccess() {
    var id =  1L;
    var response = schedulerController.delete(id);
    verify(cancelSchedule).cancel(id);
    assertThat(response.getStatus()).isEqualTo(204);
  }


}