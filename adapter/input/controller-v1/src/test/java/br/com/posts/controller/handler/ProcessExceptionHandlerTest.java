package br.com.posts.controller.handler;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.posts.controller.dto.ErrorResponseDto;
import br.com.posts.domain.exception.ProcessException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProcessExceptionHandlerTest {

  @Test
  public void whenResponseError400() {
    var handlerException = new ProcessExceptionHandler();
    var responseError = handlerException.
        toResponse(new ProcessException("Any error!"));
    assertThat(responseError.getStatus()).isEqualTo(400);
    assertThat(responseError.getEntity()).isExactlyInstanceOf(ErrorResponseDto.class);
  }
}