package br.com.posts.controller.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.posts.controller.dto.ScheduleCreateDto;
import br.com.posts.controller.dto.TypeMessageDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.ZonedDateTime;

@ExtendWith(MockitoExtension.class)
public class ControllerSchedullerMapperTest {

  private final ControllerSchedullerMapper mapper = new ControllerSchedullerMapper();

  @Test
  public void testWhenExistAllEntryParameters() {
    var scheduleDto = ScheduleCreateDto.builder()
        .messageBody("Any Message")
        .scheduleDate(ZonedDateTime.now())
        .typeMessage(TypeMessageDto.SMS)
        .recipient("16999999999")
        .build();
    var domainMessage = mapper.toDomain(scheduleDto);
    assertThat(domainMessage).isNotNull();
  }

}