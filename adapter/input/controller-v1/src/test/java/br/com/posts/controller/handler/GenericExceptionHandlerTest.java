package br.com.posts.controller.handler;

import br.com.posts.controller.dto.ErrorResponseDto;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class GenericExceptionHandlerTest {

  @Test
  public void whenResponseError500() {
    var handlerException = new GenericExceptionHandler();
    var responseError = handlerException.
        toResponse(new Exception("Any error!"));
    assertThat(responseError.getStatus()).isEqualTo(500);
    assertThat(responseError.getEntity()).isExactlyInstanceOf(ErrorResponseDto.class);
  }

}