package br.com.posts.controller.handler;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.posts.controller.dto.ErrorResponseDto;
import br.com.posts.usecase.exception.ScheduleMessageNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ScheduleMessageNotFoundExceptionHandlerTest {

  @Test
  public void whenResponseError404() {
    var handlerException= new ScheduleMessageNotFoundExceptionHandler();
    var responseError = handlerException.
      toResponse(new ScheduleMessageNotFoundException("Any error!"));
    assertThat(responseError.getStatus()).isEqualTo(404);
    assertThat(responseError.getEntity()).isExactlyInstanceOf(ErrorResponseDto.class);
  }

}