package br.com.posts.controller.handler;

import br.com.posts.controller.dto.ErrorResponseDto;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GenericExceptionHandler implements ExceptionMapper<Exception> {

  @Override
  public Response toResponse(Exception e) {
    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        .entity(ErrorResponseDto.builder()
            .developerMessage(e.getMessage())
            .userMessage("An internal error has occurred")
            .errorCode(20000).build())
        .build();
  }
}
