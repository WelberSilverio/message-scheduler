package br.com.posts.controller.handler;

import br.com.posts.controller.dto.ErrorResponseDto;
import br.com.posts.domain.exception.ProcessException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ProcessExceptionHandler implements ExceptionMapper<ProcessException> {

  @Override
  public Response toResponse(ProcessException e) {
    return Response.status(Response.Status.BAD_REQUEST)
        .entity(ErrorResponseDto.builder()
                .developerMessage(e.getMessage())
                .userMessage("Invalid request parameter")
                .errorCode(20042).build())
        .build();
  }
}
