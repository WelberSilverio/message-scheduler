package br.com.posts.controller;

import br.com.posts.controller.dto.ScheduleCreateDto;
import br.com.posts.controller.dto.ScheduleResponseDto;
import br.com.posts.controller.mapper.ControllerSchedullerMapper;
import br.com.posts.usecase.CancelSchedule;
import br.com.posts.usecase.CreateSchedule;
import br.com.posts.usecase.FindSchedule;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/scheduler")
@Produces(MediaType.APPLICATION_JSON)
public class SchedulerController {

  private final ControllerSchedullerMapper mapper;
  private final CreateSchedule createSchedule;
  private final FindSchedule findSchedule;
  private final CancelSchedule cancelSchedule;

  @Inject
  public SchedulerController(final ControllerSchedullerMapper mapper,
                             final CreateSchedule createSchedule,
                             final FindSchedule findSchedule,
                             final CancelSchedule cancelSchedule) {
    this.mapper = mapper;
    this.createSchedule = createSchedule;
    this.findSchedule = findSchedule;
    this.cancelSchedule = cancelSchedule;
  }

  @Operation(summary = "Create a new Schedule",
      responses = {
          @ApiResponse(description = "Schedule Created", responseCode = "201",
              content = @Content(mediaType = "application/json",
                  array = @ArraySchema(schema = @Schema(implementation = ScheduleResponseDto.class)))),
          @ApiResponse(description = "Bad Request", responseCode = "400"),
          @ApiResponse(description = "Internal Server Error", responseCode = "500")
      })
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public Response create(final ScheduleCreateDto scheduleDto) {
    var messageResponse = createSchedule.create(mapper.toDomain(scheduleDto));
    return Response.status(Response.Status.CREATED)
        .entity(mapper.toResponseDto(messageResponse))
        .build();
  }

  @Operation(summary = "Find a Schedule",
      responses = {
          @ApiResponse(description = "Schedule Found", responseCode = "200",
              content = @Content(mediaType = "application/json",
                  array = @ArraySchema(schema = @Schema(implementation = ScheduleResponseDto.class)))),
          @ApiResponse(description = "Not Found", responseCode = "404"),
          @ApiResponse(description = "Internal Server Error", responseCode = "500")
      })
  @GET
  @Path("/{id}")
  public Response get(@PathParam("id") final Long id) {
    var messageResponse = findSchedule.find(id);
    return Response.ok(mapper.toResponseDto(messageResponse)).build();
  }

  @Operation(summary = "Cancel a Schedule",
      responses = {
          @ApiResponse(description = "Schedule Canceled", responseCode = "204"),
          @ApiResponse(description = "Not Found", responseCode = "404"),
          @ApiResponse(description = "Internal Server Error", responseCode = "500")
      })
  @DELETE
  @Path("/{id}")
  public Response delete(@PathParam("id") final Long id) {
    cancelSchedule.cancel(id);
    return Response.noContent().build();
  }
}
