package br.com.posts.controller.mapper;

import br.com.posts.controller.dto.ScheduleCreateDto;
import br.com.posts.controller.dto.ScheduleResponseDto;
import br.com.posts.controller.dto.TypeMessageDto;
import br.com.posts.domain.ScheduleMessage;
import br.com.posts.domain.TypeMessage;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ControllerSchedullerMapper {

  public ScheduleMessage toDomain(final ScheduleCreateDto scheduleCreateDto) {
    return ScheduleMessage.builder()
        .messageBody(scheduleCreateDto.getMessageBody())
        .scheduleDate(scheduleCreateDto.getScheduleDate())
        .recipient(scheduleCreateDto.getRecipient())
        .typeMessage(TypeMessage.valueOf(scheduleCreateDto.getTypeMessage().name()))
        .build();
  }

  public ScheduleResponseDto toResponseDto(final ScheduleMessage scheduleMessage) {
    return ScheduleResponseDto.builder()
        .id(scheduleMessage.getId())
        .messageBody(scheduleMessage.getMessageBody())
        .recipient(scheduleMessage.getRecipient())
        .messageStatus(scheduleMessage.getMessageStatus().name())
        .scheduleDate(scheduleMessage.getScheduleDate())
        .typeMessage(TypeMessageDto.valueOf(scheduleMessage.getTypeMessage().name()))
        .build();
  }
}
