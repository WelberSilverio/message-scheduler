package br.com.posts.controller.handler;

import br.com.posts.controller.dto.ErrorResponseDto;
import br.com.posts.usecase.exception.ScheduleMessageNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ScheduleMessageNotFoundExceptionHandler
    implements ExceptionMapper<ScheduleMessageNotFoundException> {

  @Override
  public Response toResponse(ScheduleMessageNotFoundException e) {
    return Response.status(Response.Status.NOT_FOUND)
        .entity(ErrorResponseDto.builder()
            .developerMessage(e.getMessage())
            .userMessage("Entity not found")
            .errorCode(20023).build())
        .build();
  }
}
