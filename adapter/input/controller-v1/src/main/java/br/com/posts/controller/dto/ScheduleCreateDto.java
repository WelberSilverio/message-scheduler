package br.com.posts.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;


@Builder
@Data
public class ScheduleCreateDto {
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
  @NotNull(message = "Schedule date can't be null.")
  private ZonedDateTime scheduleDate;
  @NotBlank(message = "Message Body can't be null.")
  private String messageBody;
  @NotBlank(message = "Recipient can't be null.")
  private String recipient;
  @NotNull(message = "Type Message can't be null.")
  private TypeMessageDto typeMessage;
}
