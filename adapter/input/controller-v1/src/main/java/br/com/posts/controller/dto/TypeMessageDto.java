package br.com.posts.controller.dto;

public enum TypeMessageDto {
  WHATSAPP,
  EMAIL,
  SMS,
  PUSH
}
